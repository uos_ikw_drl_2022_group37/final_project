@tf.function
def train_step_actor(self, states):
  with tf.GradientTape() as tape:
    actions_new, log_probs = \
      self.sample_actions_form_policy(
        states)
    q1 = self._critic_1(
      (states, actions_new))
    q2 = self._critic_2(
      (states, actions_new))
    loss = tfm.reduce_mean(
      self._alpha * log_probs
      - tfm.minimum(q1, q2))
  gradients = tape.gradient(
    loss,self._actor.trainable_variables)
  self._actor.optimizer.apply_gradients(
    zip(gradients,
        self._actor.trainable_variables))