for e in range(1, epochs + 1):
  for _ in range(cycles):
    for _ in range(episodes):
      state = self._environment.reset()
      trajectory = []
      dones = False
      j = 0
      while not dones and j < t:
        actions, state_prime, r, dones = \
            self._agent.act_stochastic(
                state)
        trajectory.append((state,
                           actions,
                           state_prime,
                           dones))
        state = state_prime
        j += 1
        if self._environment.success(
                state):
          dones = True
      for i, (state,
              actions,
              state_prime,
              dones) in enumerate(
        trajectory):
        reward = self._environment.reward(
            state_prime)
        self._replay_buffer.\
          add_transition(state, actions,
                         reward,
                         state_prime,
                         dones)
        goals = \
          self._goal_sampling_strategy(
            trajectory, i,
            self._environment)
        for g in goals:
          state_new = \
              self._environment.set_goal(
                  state, g)
          state_prime_new = \
              self._environment.set_goal(
                  state_prime, g)
          reward_new = \
              self._environment.reward(
                  state_prime_new)
          self.\
            _replay_buffer.add_transition(
            state_new, actions,
            reward_new, state_prime_new,
            dones)
    if self._replay_buffer.ready():
      for i in range(n):
        self._agent.learn()