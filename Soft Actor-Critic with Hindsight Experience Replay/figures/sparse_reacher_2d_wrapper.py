def desired_goal(self, state):
  return state[4], state[5]

def achieved_goal(self, state):
  x_t, y_t = self.desired_goal(state)
  x_v, y_v = state[8], state[9]
  x_g, y_g = x_v + x_t, y_v + y_t
  return x_g, y_g

def set_goal(self, state, goal):
  x_g, y_g = goal
  x, y = self.achieved_goal(state)
  new_state = np.array(state)
  new_state[4], new_state[5] = x_g, y_g
  new_state[8], new_state[9] = (x - x_g,
                                y - y_g)
  return new_state

def reward(self, state):
  return -1 if np.linalg.norm(
    [state[8], state[9]]) \
               > self._delta else 0