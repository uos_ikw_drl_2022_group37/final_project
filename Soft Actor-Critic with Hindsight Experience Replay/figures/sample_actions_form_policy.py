@tf.function
def sample_actions_form_policy(
        self, state):
  mu, sigma = self._actor(state)
  distribution = tfd.Normal(mu, sigma)
  actions = distribution.sample()
  log_probs = distribution.log_prob(
    actions)
  actions = tfm.tanh(actions)
  log_probs -= tfm.log(1 - tfm.pow(
    actions, 2) + 1e-6)
  log_probs = tfm.reduce_sum(
    log_probs, axis=-1, keepdims=True)
  return actions, log_probs