@tf.function
def train_step_critic(self, states,
                      actions, rewards,
                      states_prime,
                      dones):
  actions_prime, log_probs = \
    self.sample_actions_form_policy(
      states_prime)
  q1 = self._critic_1_t((states_prime,
                         actions_prime))
  q2 = self._critic_2_t((states_prime,
                         actions_prime))
  q_r = tfm.minimum(q1, q2) \
        - self._alpha * log_probs
  targets = self._reward_scale * rewards \
            + self._gamma \
            * (1 - dones) * q_r
  self._critic_update(self._critic_1,
                      states, actions,
                      targets)
  self._critic_update(self._critic_2,
                      states, actions,
                      targets)


def _critic_update(self, critic, states,
                   actions, targets):
  with tf.GradientTape() as tape:
    q = critic((states, actions))
    loss = 0.5 * self._mse(targets, q)
  gradients = tape.gradient(
    loss, critic.trainable_variables)
  critic.optimizer.apply_gradients(
    zip(gradients,
        critic.trainable_variables))