\section{Implementation} \label{Implementation}

In this section, we discuss the implementation of our approach. This implementation consists of multiple components. The most important ones are the \ac{sac} agent and their neural networks for the actor and critic, the \ac{her} training module, a standard \ac{erb} and environment wrapper classes. Our full code is available under \cite{proj_git}.

\subsection{Experience Replay Buffer}

The \ac{erb} stores transitions form the environment for training. A transition consists of the current environment state, the next action, the following state by taking the action, the resulting reward and a boolean flag that is true if the resulting state is a terminal state. Transitions can be added with the \texttt{add\_transition} method. For training of an of policy \ac{rl} algorithm, a batch of transitions is randomly sampled from the buffer. This is done by the \texttt{sample\_batch} method. The size of a batch is set by the \texttt{batch\_size} parameter. Because of limited memory, a buffer can only hold a certain amount of elements. The maximum elements the \ac{erb} can hold is determined by the \texttt{max\_size} parameter. After its maximum size is reached, the oldest items in the replay buffer are replaced by the newly added.

\subsection{SAC+HER}

\ac{sac} was introduced in \cite{SAC1} and uses, besides the actor network and $\mathrm{Q}$-Networks, one additional value network. Our implementation of \ac{sac} is a more modern version conforming to the specification of \cite{SAC_OpenAI}, that does not use an additional value network.

\ac{her} was first presented by \cite{HER1} where it was implemented using \ac{ddpg} to find the optimal policy. There are already a few publications towards the combination of \ac{sac} and \ac{her}\\ (\cite{SAC_HER1}, \cite{SAC_HER2},\cite{SAC_HER3}) of which we focused on \cite{SAC_HER3}.

To solve Sparse reward settings, \ac{her} replays failed episodes with relabeled goal states. At first, our \ac{sac}+\ac{her} algorithm plays a certain number of episodes in the environment by following the policy of the \ac{sac} agent. For each episode, all observed transitions are added to a \ac{erb}. Next, a set of additional goals are determined for every transition in the episode. For each sampled goal, the desired goal of the transition is replaced with the new goal and then added to the replay buffer. Finally, a fixed number of \ac{sac} update steps are performed. The whole procedure is done for a given amount of cycles and epochs. Our \ac{sac}+\ac{her} version is displayed in \autoref{alg:sac_her}.

\begin{algorithm*}
\DontPrintSemicolon
  \KwInput{ \\
      - initial policy parameters $\theta$, $\mathrm{Q}$-function parameters $\phi_1$, $\phi_2$ \;
      - a strategy $\mathbb{S}$ for sampling goals for replay \;
      - a reward function $r: \mathcal{S} \times \mathcal{A} \times \mathcal{G} \rightarrow \mathbb{R}$ \;
  }
  Set target parameters equal to main parameters: $\phi_{targ, 1} \leftarrow \phi_1, \phi_{targ, 2} \leftarrow \phi_2$ \;
  Initialize replay buffer $R$ \;
  
  \For{epoch=1, E}{
      \For{cycle=1, C}{
        \For{episode=1, M}{
          Sample a goal $g$ and an initial state $s_0$ \;
          \For{t=0, T-1}{
            Sample an action $a_t$ using the policy $\pi_\theta$: $a_t=\pi_\theta(s_t)$ \;
            Execute action $a_t$ and observe new state $s_t$
          }
          \For{t=0, T-1}{
            $r_t := r(s_t, a_t, g)$ \;
            Store the transition $(s_t||g, a_t, r_t, s_{t+1}||g, d_t)$ in $R$\;
            Sample a set $G$ of additional goals for replay \;
            \For{$g' \in G$}{
              $r' := r(s_t, a_t, g')$ \;
              Store the transition $(s_t||g', a_t, r_t, s_{t+1}||g', d_t)$ in $R$ \;
            }
          }
        }
        \For{t=1, N}{
          Randomly sample a batch $B$ of transitions from $R$ \;
          Compute the targets for the $\mathrm{Q}$-functions:       $y(r,s',d)=r+\gamma(1-d)(\min_{j=1,2}Q_{\phi_{targ,j}}(s',\tilde{a}') - \alpha log\pi_\theta(\tilde{a}'|s'))$,  $\tilde{a}' \sim \pi_\theta(\cdot|s')$ \;
          Update $\mathrm{Q}$-functions by one step of gradient decent using
          $\nabla_{\phi_i} \sum \frac{1}{|B|} \sum_{(s, a, r, s', d) \in B} (\mathrm{Q}_{\phi_i} (s, a) - y(r,s',d))^2 \textrm{, for } i = 1, 2$ \;
          Update policy by one step of gradient assent using 
          $\nabla_\theta \sum \frac{1}{|B|} \sum_{s \in B} (\min_{i=1, 2}\mathrm{Q}_{\phi_i} (s, \tilde{a}_\theta(s)) - \alpha log\pi_\theta(\tilde{a}_\theta(s)|s)))$,
          Where $\tilde{a}_\theta(s)$ is a sample form $\pi_\theta(\cdot|s)$ which is differentiable wrt $\theta$ via the reparametrization trick. \;
          Update target networks with
          $\phi_{targ, i} \leftarrow (1-\rho) \phi_{targ, i} + \rho \phi\textrm{, for } i = 1, 2$ \;
        }
      }
  }
\caption{SAC+HER}
\label{alg:sac_her}
\centering
\end{algorithm*}

At first, in an update step of \ac{sac}, a batch of transitions is sampled from the \ac{erb}. For each network, a gradient update is done based on the batch. Our implementation of \ac{sac} uses two $\mathrm{Q}$-Networks. To determine a $\mathrm{Q}$ value for a state action combination, the minimum of the resulting values of the two networks is used. This is done to limit the overestimation bias of $\mathrm{Q}$-Networks. The update formulas of the $\mathrm{Q}$-Networks are displayed in line 19 and 20 in \autoref{alg:sac_her}. The other type of network that has to be trained is the actor (policy) Network. The update of this network is done by the formula in line 21. To balance the weight of reward and entropy, a parameter $\alpha$ is used in the updates. The original \ac{her} algorithm from \cite{HER1} uses a reward scaling factor, that is multiplied with the reward. The $\alpha$ value should be the inverse of the original reward scaling. 

To sample actions, \ac{sac} uses a normal distribution, that is also used to calculate the log probabilities of the corresponding actions. Action values are squashed by a \textit{tanh} function to stay in an interval between -1 and 1. Because of the squashing, the log probabilities of the actions need to be adjusted to the new interval. The Python code for implementing the sampling of actions and calculating their log probabilities in TensorFlow 2 is shown in listing \ref{listing:sample_actions_form_policy}. Before action values are passed into the environment, they need to be scaled appropriately if the environment needs values different from the -1 to 1 interval.

There are different ways to sample a set of additional goals, these are described in \autoref{goal_sampling}. We implemented the \textit{future} and \textit{final} strategies from \cite{HER1}. Additionally, we implemented a new goal sampling strategy called \textit{k final}. It is basically the combination of the \textit{final} goal sampling with the addition of the \textit{k} parameter that controls the number of relabeled transitions in the \ac{erb}.

\begin{listing}
\inputminted{octave}{figures/sample_actions_form_policy.py}
\caption{The \ac{sac} agent's Method to sample and squash actions and their log probabilities form the policy}
\label{listing:sample_actions_form_policy}
\end{listing}

\subsection{Environment Wrappers}

For our evaluation, we need sparse binary reward environments. This is given in the panda-gym reacher environment. To achieve this in  the MuJoCo reacher environment, a wrapper class is created to convert the dense shaped rewards to sparse binary rewards. The calculation of rewards conforms to the formula:

$r_{t}=r_{g}\left(s_{t}, a_{t}, g\right)=\left\{\begin{array}{c}0, \text { if }\left|s_{t}-g\right|<\delta \\ -1, \text { otherwise }\end{array}\right.$
\cite{HER1}

Thereby, the goal is considered fulfilled if the distance between the desired goal (point to reach) and the achieved goal (position of the reachers fingertip) is smaller than a $\delta$. For the reacher environment, we use $\delta=0.015$ as default. If the goal is satisfied, a reward of 0 is returned, otherwise a reward of -1. The python implementation can be fund in the \texttt{reward} method in listing \ref{listing:sparse_reacher_2d_wrapper}.

Additionally, \ac{her} needs the desired goal and the achieved goal in the current time step from the environment. This information is encoded in the observation data of the reacher. To extract the desired goal and the achieved goal from the observation, the wrapper class includes the functions \texttt{achieved\_goal} and \texttt{desired\_goal}. The desired goal (the coordinates of the desired point to be reached) can be directly extracted from the observation. In contrast, the achieved goal has to be calculated, because the observation does not contain the coordinates of the fingertip of the reacher. Instead, it includes the vector from the fingertip to target goal. In \ac{her} goal states are altered, to provide this functionality, the method \texttt{set\_goal} is created. It takes an observation and a goal to be set. Because the reacher observation contains the fingertip to target goal vector, this information has also to be adjusted to the new goal. Listing \ref{listing:sparse_reacher_2d_wrapper} contains the python code of the reacher wrapper environments methods.

\begin{listing}
\inputminted{octave}{figures/sparse_reacher_2d_wrapper.py}
\caption{Methods of the sparse reacher 2d wrapper class.}
\label{listing:sparse_reacher_2d_wrapper}
\end{listing}

\subsection{Networks}

Our implementation of an \ac{sac} agent needs two neuronal networks for training, an actor, and a critic network. For the reach tasks we used a classical \ac{mlp} consisting out of 3 hidden layers with 256 neurons. Each hidden layer uses an \ac{relu} activation functions. The critic network is a $\mathrm{Q}$-Network, that predicts the Q-Value of the given state and action pair. Before fed into the \ac{mlp}, state and action are concatenated. The output layer consists of one single neuron with a linear activation function. \ac{sac} uses a stochastic policy based on a normal distribution. This needs the mean $\mu$ and standard divination $\sigma$ as parameters. To determine these parameters, the actor network gets a state for that the best next action should be predicted. As output layers for $\mu$ and $\sigma$, two layers with a single neuron are used. The layer for $\mu$ has a linear and the layer for $\sigma$ has a \textit{softplus} output function. \textit{Softplus} is chosen for $\sigma$ because the standard deviation needs to be a positive value greater than zero. The Python code for creating the actor and critic network is shown in listing \ref{listing:networks}.

\begin{listing}
\inputminted{octave}{figures/networks.py}
\caption{Functions to create the actor (policy network) and critics ($\mathrm{Q}$-networks) for the \ac{sac} agent.}
\label{listing:networks}
\end{listing}