\section{Evaluation} \label{Evaluation}
In the following, we will go in detail over the results of our experiments and evaluate our findings. The detailed implementation and setup of the experiments in the two environments is explained in \autoref{Implementation}.
For this, we compared the performance of \ac{sac} and \ac{sac}+\ac{her} in \cite{reacher_3d_env}, presented in \autoref{figure:sac_her_dense_sparse_success}. Additionally, we compared the performance of \ac{sac}+\ac{her} and the different goal sampling strategies described in \cite{reacher_2d_env} respectively, visible in \autoref{figure:final_k_final_future_average_returns} and \autoref{figure:final_k_final_future_success}. Each experiment and setup ran five times over 40 epochs.

\subsection{SAC+HER sparse vs SAC sparse vs SAC dense}
To have a sufficient baseline comparison for our \ac{sac}+\ac{her} approach, we choose vanilla \ac{sac} and \ac{sac} with reward shaping. The reward shaping was realized by using the dense environment, compared to the sparse environment used to the other two approaches. Specially, we used the panda-gym reacher environment \cite{reacher_3d_env}, which is by design sparse. We expected vanilla \ac{sac} not to perform well in this environment and both \ac{sac} with reward shaping and \ac{sac}+\ac{her} to outperform this basic approach.  \ac{sac}+\ac{her} uses the best sampling strategy from the original \ac{her} paper \cite{HER1}, which is the \textit{future} goal sampling strategy. \\

We display our results in \autoref{figure:sac_her_dense_sparse_success} it provides the success rate of the approaches over the epochs.
\ac{sac}+\ac{her} is able to solve the task after the first epoch consistently and stable in each following epoch. \ac{sac} similarly also manages to achieve very high success rates after the first epoch, however the success rates are less stable than \ac{sac}+\ac{her}.
Pure \ac{sac} never manages to complete the environment within the 40 epochs, and it's success rate also never exceeds a value of 0.1.

To summarize, while \ac{her} with reward shaping manages to solve the environment with sparse rewards in most cases, \ac{sac}+\ac{her} outperforms it by reaching consistent and high success rates in all epochs. This is in line with our expectations beforehand, as \ac{sac}+\ac{her} was shown to outperform pure \ac{sac} \cite{SAC_HER1},\cite{SAC_HER2},\cite{SAC_HER3}.

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{figures/sac_her_dense_sparse_success.pdf}
  \caption{Comparison of the success rates of \ac{sac}+\ac{her} with spars binary rewards, pure \ac{sac} with spars binary rewards and \ac{sac} with dense shaped rewards over 40 epochs in the panda gym reacher environment. For each test setup, 5 test runs were done, and the results are displayed with their mean and error interval (max and min value).}
  \label{figure:sac_her_dense_sparse_success}
\end{figure}

\subsection{final vs k final vs future goal sampling}

\autoref{figure:final_k_final_future_success} provides the success rates of \ac{sac}+\ac{her} using either \textit{final}, \textit{k final}, or \textit{future} as the respective goal sampling strategies.
For our testing of the goal sampling strategies with \ac{sac}+\ac{her}, we used a sparse reward wrapper for the MuJoCo's 2D reacher environment \cite{reacher_2d_env}. As mentioned before, we selected \textit{k=4} for both the \textit{future} and the \textit{k final} strategies.
We expected \textit{k final} to outperform \textit{final} in the 2D reacher environment. In order to be able to further evaluate the performance of \textit{k final}, we also compared in to the performance of \textit{future}, the goal sampling strategy that was previously shown to be the most performant in \cite{HER1}. This is especially important, as we wanted to test, if \textit{final} also using a parameter \textit{k} would perform similar to \textit{future}.

\autoref{figure:final_k_final_future_success} overall shows, that \ac{sac}+\ac{her}, using \textit{future} as the goal sampling strategy, outperforms both \textit{k final} and \textit{final} regarding the success rates. It reaches higher success rates in way earlier epochs than the other two goal sampling strategies. After the 15th epoch, \ac{sac}+\ac{her} using \textit{future} almost consistently reaches a success rate of 1.
\ac{sac}+\ac{her} using the \textit{k final} goal-sampling strategy 
outperforms the \textit{final} strategy after the 4th epoch, reaching a success rate of 1 earlier than \textit{final}. In the first four epochs, \textit{final} and \textit{k final} perform very similar.

\autoref{figure:final_k_final_future_average_returns} shows the average return for \ac{sac}+\ac{her} using one of the three goal sampling strategies \textit{future}, \textit{k final} or \textit{final}. It also shows that, in respect to the average returns, \textit{k final} outperforms \textit{final} regarding both speed and maximal achieved average return after the first 4 epochs, where they perform equally. \textit{k final} achieves the maximal value after approximately 22 epochs consistently, while \textit{final} only reaches such values at the end of the 40 epochs.
However, \textit{k final} still gets outperformed by \textit{future}, reaching a higher average return and also reaching this value significantly faster \autoref{figure:final_k_final_future_average_returns}.

While \textit{k final} outperforms \textit{final} both in terms of success rates and average returns significantly, it still gets outperformed by \textit{future} in both of these categories. This is in line with our initial expectations regarding the performance of \textit{k final} over \textit{final}. While \textit{future} outperforms \textit{k final}, it still performs significantly better than \textit{final}. Therefore, with the introduction of the parameter \textit{k}, we managed to yield better performance than \textit{final}, but it still falls short behind \textit{future}, which also uses the parameter \textit{k}.

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{figures/final_k_final_future_success.pdf}
  \caption{Comparison of the success rates of \ac{sac}+\ac{her} with different goal sampling strategies (\textit{final}, \textit{k final} and \textit{future}) over 40 epochs in the MuJoCo reacher environment with our sparse reward wrapper. The \textit{k final} and \textit{future} both use 4 as \textit{k} parameter. For each test setup, 5 test runs were done, and the results are displayed with their mean and error interval (max and min value).}
  \label{figure:final_k_final_future_success}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{figures/final_k_final_future_average_returns.pdf}
  \caption{Comparison of the average returns of \ac{sac}+\ac{her} with different goal sampling strategies (\textit{final}, \textit{k final} and \textit{future}) over 40 epochs in the MuJoCo reacher environment with our sparse reward wrapper. The \textit{k final} and \textit{future} both use 4 as \textit{k} parameter. For each test setup, 5 test runs were done, and the results are displayed with their mean and error interval (max and min value).}
  \label{figure:final_k_final_future_average_returns}
\end{figure}
