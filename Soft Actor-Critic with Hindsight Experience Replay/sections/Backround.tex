\section{Background} \label{Background}

In the following, we will introduce the work and methods that we used in this paper. Will we explain and shortly discuss \ac{sac} and \ac{her}, the two parts of our learning algorithm. Furthermore, we will introduce the two environments that we used in our experiments.


\subsection{Soft Actor-Critic} \label{sac_section}
\ac{sac} is an off-policy deep reinforcement learning algorithm and optimizes a stochastic policy. \cite{SAC1} firstly introduced the method, we will orient ourselves and use the modern version of \cite{SAC_OpenAI}. Our version works in continuous environments, but there are also variants that work in discrete environments. It is related to \ac{ddpg} and stochastic policy optimization approaches. An actor and a critic network are trained, where the actor network learns to act on the policy and the critic network rates the performance. \ac{sac} employs entropy regularization as one of its central measures. There is the objective of finding the optimal policy that maximizes expected long term reward and long term entropy \cite{seita_soft_2018}. The overarching concept here is called the exploration-exploitation trade-off: It is often beneficial for an agent to explore the environment early on and the use the collected knowledge to exploit the best strategy. The entropy can be seen as a measure of randomness in the policy, it is added to the Q-loss $Q*(s,a)$ and the value function $V*(s)$ as the expected future entropy. 
Specifically, \ac{sac} uses a soft state-value function ($V(s_t)$) and a soft Q-value function ($Q(s_t,a_t)$). They are defined as:

\begin{equation*}
    \begin{aligned}
    V(s_t) = \mathbb{E}_{a_t~\pi}[Q(s_t,a_t) -log \pi(a_t|s_t)] \\ 
    Q(s_t,a_t) = r(s_t,a_t) + \gamma \mathbb{E}_{s_{t+1}~p}[V(s_{t+1}]
    \end{aligned}
\end{equation*}

{\footnotesize defined in \cite[p.4]{SAC1}}

 
Here, $V(s_t)$ is the expected sum of the reward for a current state $s_t$, while $Q(s_t,a_t)$ is the expected sum of the reward for a state-action pair of state $s_t$ and action $a_t$.

In \ac{sac} one policy $\pi_{\theta}$ and two Q-functions $Q_{\phi_1}$, $Q_{\phi_2}$ are learned. 



The loss functions for Q-networks in \ac{sac} are defined as:
\[ L(\phi_1,D) = E_{(s,a,r,s',d)~D} [(Q_{\phi_i}(s,a)-y(r,s',d))^2 ] \]

where the target is given by:

\begin{equation*}
\begin{aligned}
   y(r,s',d) = & r + \gamma(1-d)(\min_{j=1,2}Q_{\phi_{targ,j}}(s',\tilde{a}') \\
   & -alog\pi_\theta(\tilde{a}'|s')),  \tilde{a}' \sim\pi_\theta(\cdot|s') 
\end{aligned}
\end{equation*}

The samples for \ac{sac} are selected according to a squashed Gaussian policy:
\[ \tilde{a}_\theta(s,\xi) = tanh(\mu_\theta(s) + \sigma_\theta(s)\odot \xi),   \xi ~ N(0,I). \]

\begin{equation*}
    \begin{aligned}
        V^{\pi}(s) = \underset{a \sim \pi}{E} {Q^{\pi}(s,a)} + \alpha H\left(\pi(\cdot|s)\right) \\ = \underset{a \sim \pi}{E} {Q^{\pi}(s,a) - \alpha \log \pi(a|s)}\\
= \max_{\theta} \underset{\xi \sim \mathcal{N}}{\underset{{s \sim \mathcal{D}}}{E}} \substack{\min_{j=1,2}} Q_{\phi_j}(s,\tilde{a}_{\theta}(s,\xi)) - \alpha \log \pi_{\theta}(\tilde{a}_{\theta}(s,\xi)|s)}
    \end{aligned}
\end{equation*}

\subsection{Hindsight Experience Replay} \label{her_section}
\ac{her} was introduced in \cite{HER1}.
\ac{her} uses experienced episodes to learn from the previous actions for the next episode. It is a sample-efficient replay method, usually applied to off-policy \ac{drl} algorithms.
It tries to solve the challenge concerned with sparse and binary rewards in \ac{rl}. Additionally, it is also very useful for multi-goal scenarios.
The idea behind the \ac{her} algorithm is that it saves additional transitions into the replay buffer. These are generated with the rewards on goals, which are additionally chosen on the episode.  Using this, the agent can learn from failed attempts of reaching the goal in order to improve the next attempts. This works only for off-policy algorithms, since only they allow learning on the past interaction with the environment, without changing the acting policy.
The \ac{her} algorithm sets a reached state in a previous transition as an alternative goal to learn from that. This does not help the agent to learn how to reach the initial goal directly, but the agent learns about the environment. Because it can also reach neighboring states of that state, eventually it will reach the goal state by replaying the already obtained outcomes.
Therefore, the algorithm will learn, by training on this replay buffer with the positive reward, acquired by the set alternative goals. It will propagate value trough generalization in the state-action-goal space.
Henceforth, with \ac{her}, the algorithm can learn from both failures and from successes.


\subsection{Goal sampling strategies}
\label{goal_sampling}

The researchers in \cite{HER1} applied different strategies of how to choose the goal and evaluated the strategies performances against each other. Three of the four strategies used have parameter \textit{k} which controls how many states to replay. 
\begin{itemize}
    \item The \textit{finale} strategy uses the only final state of the environment, there is no parameter \textit{k}.
    \item The \textit{future} strategy replays \textit{k} random states, which were observed after the transition and come from the same episode as the transition 
    \item The \textit{episode} strategy replays \textit{k} random states, which come from the same episode as the transition being replayed
    \item The \textit{random} strategy samples \textit{k} random states from the whole past training for the replay
\end{itemize}

To compare the performance, three tasks were looked at. For each of the tasks, the parameter \textit{k} was changed. Most of the time, raising \textit{k} over 8 decreasd the performance. Consistently, the \textit{random} strategy yielded the worst performance. While for the performance between the other strategies is not always clearly different, the \textit{future} strategy is the only strategy being able to solve the sliding task almost perfectly. However, the highest performance of all strategies (expect \textit{random}) are similar in the pushing and pick-and-place and all strategies are able to solve this task almost consistently.

\subsection{Limitations of Hindsight Experience Replay}

The \ac{her} implementation described in \cite{HER1} might have the pitfalls of instability and local optimality as described by \cite{SAC_HER3}. The researchers solved these shortcomings by combining \ac{her} with MERL (Maximum Entropy Reinforcement Learning), which led to better performance. They called their approach Soft Hindsight Experience Replay.
In addition, \ac{her} can not be used with every environment. Often, variants of \ac{her} need to be defined for unsupported environments. As an example, \ac{her} cannot be directly applied to linguistic goals \cite{Cideron2020HIGhERII}.
Concluding, \ac{her} has some limitations, but there is prior work suggesting improvement in performance when using \ac{sac} on an appropriate environment.

\subsection{Problems with Reward Shaping} \label{problems_reward_shaping}

Reward shaping in an environment agnostic-form is not able to solve the problems reliably. To reach the desired performance, often a high level of domain specific knowledge is needed and the effort of creating the highly specialized rewards might make the training of policy unnecessary compared to hard coding the policy in the first place. This was already discussed in \cite{HER1}.

\subsection{Reacher Environments}

\subsubsection{MuJoCo Reacher}

\begin{figure}[htbp]
\centerline{\includegraphics[scale=0.65]{figures/reacher.png}}
\caption{MuJoCo Reacher environment \cite{reacher_2d_env}.}
\label{Reacher_2d}
\end{figure}

In the MuJoCo reacher environment (\cite{reacher_2d_env}), a two-jointed robotic arm must position the end point of the arm at a target position. We used the version "Reacher-v4" for our tests \cite{reacher_2d_env}.

\subsubsection{Panda-gym reach}

\begin{figure}[htbp]
\centerline{\includegraphics[scale=0.5]{figures/reacher_3d.png}}
\caption{Panda-gym reach environment \cite{reacher_3d_env}.}
\label{Reacher_3d}
\end{figure}

In the panda-gym Reacher 3D environment, a robotic arm must position its end point at the target position \cite{reacher_3d_env}.
For the sparse reward we used "PandaReach-v2" and for the dense reward we used "PandaReachDense-v2".
By default, for all panda-gym environments, the reward is a sparse reward. Therefore, if the agent has reached the desired location the reward is 0, otherwise the reward is -1. The agent has a default tolerance of 5 cm for the goal state (\cite{Gallouedec2021pandagymOG}).
For the dense reward, the reward is the opposite of the distance between the agent and the goal state (\cite{Gallouedec2021pandagymOG}).
These sparse and dense rewards were also used in our experiment runs.