## SAC (Soft actor-critic)
- optimize  J(π) = T∑_t=0 E(st,at)∼ρπ [r(st, at) + αH(π(·|st))]

![](SAC_pseudocode.png)
 

#### main objective: 
- find optimal policy π∗ that maximizes entropy-augmented reward function, which requires the soft policy iteration of soft Q- and value functions

##### Code examples
- https://github.com/ADGEfficiency/soft-actor-critic
- https://github.com/shakti365/soft-actor-critic

## HER (hindsight experience replay)
- https://proceedings.neurips.cc/paper/2017/file/453fadbd8a1a3af50a9df4df899537b5-Paper.pdf

#### main idea: 
- allow DRL agents to elarn from both failures and successes
- employs concept of goal, that the agent has to achieve in environment
-> The closer the state st is to the goal g, the greater the
reward the DRL agent receives.

![](HER_pseudocode.png)


### Sources
- https://arxiv.org/pdf/2106.01016v2.pdf
- https://spinningup.openai.com/en/latest/algorithms/sac.html
- https://arxiv.org/abs/1801.01290
- https://towardsdatascience.com/reinforcement-learning-with-hindsight-experience-replay-1fee5704f2f8
- https://medium.com/this-week-in-machine-learning-ai/reinforcement-learning-deep-dive-feat-pieter-abbeel-9375caca859