import tensorflow as tf
import numpy as np
import random
import matplotlib.pyplot as plt

from stable_baselines import SAC
# see: https://stable-baselines.readthedocs.io/en/master/modules/her.html
# see colab: https://colab.research.google.com/github/Stable-Baselines-Team/rl-colab-notebooks/blob/master/stable_baselines_her.ipynb#scrollTo=qPg7pyvK_Emi

model_class = SAC


class Envir():
    """
    Environment for HER, might be replaced with env we select
    """
    def __init__(self, size, rand_s = True):
        self.size = size
        self.goal = np.random.randInt(3, size=size) 
        self.start = np.random.randInt(3, size=size)
        while np.sum(self.start == self.goal) == size:
            self.goal = np.random.randint(3, size = size)

        self.state = self.start

        if rand_s:
            self.start_backup = self.start
            self.goal_backup = self.start

    def step(self, action):
        # ToDo: code action on state
        return

    def reset(self, rand_s):
        if rand_s:
            self.start = np.random.randInt(3, size=size)
            self.goal = np.random.randInt(3, size=size) 
        else:
            self.start = self.start_backup
            self.goal = self.goal_backup


"""
Memory of HER, for storing transitions
"""
class Memory():
    # init memory
    def __init__(self):
        self.memory = []
    
    def memorize(self, goal, state, action, reward, next_step, finished):
        self.memory += [(goal, state, action, reward, next_step, finished)]

    def reset_memory(self):
        self.memory = []

class HER_buffer():
    """
    HER Buffer
    """

    def __init__(self, envir, buffer_size, memory: Memory):
        self.envir = envir
        self.buffer_size = buffer_size
        self.memory = memory
        self.full = False
        self.episode 
        
        # set action, reward and observation spaces of buffer
        self.actions = np.zeros((self.buffer_size,) + get_space_shape(envir.action_space), dtype = envir.action_shape.dtype)
        self.observations = np.zeros((self.buffer_size,) + get_space_shape(envir.observation_space), dtype = envir.observation_space.dtype)
        self.rewards = np.zeros((self.buffer_size, 1), dtype=np.float32)

        self.goals = np.zeros((self.buffer_size,) + get_space_shape(envir.observation_space), dtype = envir.observation_space.dtype)
        self.achieved_goals = self.goals

    def remember_transition(self, transition):
        self.memory.memorize() # ToDo: add stuff to memorize

    def choose_next_a(self, state, goal):
        return


    def sample(self):     
        sampled = self.buffer_size
        # Sample transitions from last complete episode

        #if self.full:

        #else:

        return sampled

    def sample_init_state(self, batch_size):
        return

    def sample_action(self,batch_size , behav_policy):
        return

    def sample_goal(self, batch_size):
        return

    def sample_add_goals(self, batch_size):
        return



# train model

    """
    for episode:
        sample goal and initial state
            sample action (based on behaviour policy)
            execute action and obsever new state

    for range T:
        store transition
        sample set of addit. goals for replay

        for g' do:
            store transition of g'
        
    
    for t do:
        sample minibatch from replay buffer
        perform one step of optimizing unding A and minibatch

    """


